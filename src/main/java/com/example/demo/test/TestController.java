package com.example.demo.test;

import com.example.demo.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "api/test")
public class TestController {
    private final static String URI_STUDENTS = "http://localhost:8080/api/v1/student";

//    @Autowired
    RestTemplate restTemplate;

    @GetMapping
    public ResponseEntity<Student[]> test() {
        restTemplate = new RestTemplate();
        Student[] result = restTemplate.getForObject(URI_STUDENTS, Student[].class);
        System.out.println(result[0]);

        ResponseEntity<Student[]> students1 = restTemplate.getForEntity(URI_STUDENTS, Student[].class);
        System.out.println(students1);

		return students1;
    }
}
