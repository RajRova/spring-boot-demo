package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student coco = new Student(
                    "Coco",
                    "coco@lapin.pin",
                    LocalDate.of(2000, Month.JANUARY, 5)
            );

            Student gogo = new Student(
                    "Gogo",
                    "gogo@pinou.pon",
                    LocalDate.of(2001, Month.JANUARY, 5)
            );

            repository.saveAll(List.of(coco, gogo));
        };
    }
}
